<?php 

//
// 
// on upload, automatically add image title and alt
// 
//
add_action( 'add_attachment', 'my_set_image_meta_upon_image_upload' );
function my_set_image_meta_upon_image_upload( $post_ID ) {

  // Check if uploaded file is an image, else do nothing

	if ( wp_attachment_is_image( $post_ID ) ) {

		$my_image_title = get_post( $post_ID )->post_title;

		// Sanitize the title:  remove hyphens, underscores & extra spaces:
		$my_image_title = preg_replace( '%\s*[-_\s]+\s*%', ' ',  $my_image_title );

		// Sanitize the title:  capitalize first letter of every word (other letters lower case):
		$my_image_title = ucwords( strtolower( $my_image_title ) );

		// Create an array with the image meta (Title, Caption, Description) to be updated
		// Note:  comment out the Excerpt/Caption or Content/Description lines if not needed
		$my_image_meta = array(
			'ID'    => $post_ID,      // Specify the image (ID) to be updated
			'post_title'  => $my_image_title,   // Set image Title to sanitized title
			// 'post_excerpt'  => $my_image_title,   // Set image Caption (Excerpt) to sanitized title
			// 'post_content'  => $my_image_title,   // Set image Description (Content) to sanitized title
		);

		// Set the image Alt-Text
		update_post_meta( $post_ID, '_wp_attachment_image_alt', $my_image_title );

		// Set the image meta (e.g. Title, Excerpt, Content)
		wp_update_post( $my_image_meta );

	} 
}



// 
// 
// set updated featured image w/ acf image
// 
// 
function acf_set_featured_image( $value, $post_id, $field  ){
	$id = $value;
	if( ! is_numeric( $id ) ){
		$data = json_decode( stripcslashes($id), true );
		$id = $data['cropped_image'];
	}
	update_post_meta( $post_id, '_thumbnail_id', $id );
	return $value;
}

// acf/update_value/name={$field_name} - filter for a specific field based on it's name
add_filter( 'acf/update_value/name=featured_image_acf', 'acf_set_featured_image', 10, 3 );


//
// 
// make jpeg quality 100 be default, smush after
// 
//
add_filter('jpeg_quality', function($arg){return 100;});


//
// 
// enque scripts
// 
//
add_action( 'wp_enqueue_scripts', 'fancySquresUnsung_enqueue_scripts' );

function fancySquresUnsung_enqueue_scripts() {
   

    if(!is_front_page()){
		wp_enqueue_script( 'wp-mediaelement' );
		wp_enqueue_style( 'wp-mediaelement' );
		wp_enqueue_script( 'mediaelement-vimeo' );
    }


}