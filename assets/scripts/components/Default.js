function fancySquaresHeaderScroll(){
	var docElem = document.documentElement,
		header = $('.header--main'),
		didScroll = false,
		changeHeaderOn = 150;

	function fireScrol() {
		window.addEventListener( 'scroll', function( event ) {
			if( !didScroll ) {
				didScroll = true;
				setTimeout( scrollPage, 250 );
			}
		}, false );
	}

	function scrollPage() {
		var sy = scrollY();
		if ( sy >= changeHeaderOn ) {
			header.addClass( 'header--bg-color' );
			// $("#block-block-11").animate({opacity: 0}, 'fast');

			
			// console.log('test');
		}
		else {
			header.removeClass( 'header--bg-color' );
			// $("#block-block-11").animate({opacity: 1}, ''fast'');
			
		}
		didScroll = false;
	}

	function scrollY() {
		return window.pageYOffset || docElem.scrollTop;
	}

	fireScrol();
}


function fs_owl_slider(){
	let $pageSlider = $('.owl-carousel--testimonial');

	$pageSlider.owlCarousel({
		loop:true,
		autoplay:true,
		margin:10,
		nav:false,
		autoHeight:true,
		autoplayTimeout:10000,
		items: 1
	})
}

function fs_home_intro_slider(){
	let $introSlider = $('.owl-carousel--intro');

	$introSlider.owlCarousel({
		loop:true,
		autoplay:true,
		margin:0,
		nav:false,
		dots: true,
		autoplayTimeout:7000,
		autoHeight:true,
		items: 1,
		remove: 0
	});
	
	if(Foundation.MediaQuery.is('small only')){
		$introSlider.owlCarousel('remove', 0);
		// console.log('large!');
	}

	// console.log('test');
}

function fs_video_element(){
	let $video = $('video');

	if ($video.length > 0) {
		$video.mediaelementplayer({
			// Do not forget to put a final slash (/)
			pluginPath: 'https://cdnjs.com/libraries/mediaelement/',
			// this will allow the CDN to use Flash without restrictions
			// (by default, this is set as `sameDomain`)
			shimScriptAccess: 'always'
			// more configuration
		});
	}
}

module.exports = {
	fancySquaresHeaderScroll,
	fs_owl_slider,
	fs_video_element,
	fs_home_intro_slider
};