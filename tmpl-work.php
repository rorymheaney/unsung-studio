<?php
/**
 * Template Name: Work
 * Description: Work Page Template - see pages and partials
 */
// $start = TimberHelper::start_timer();
$context = Timber::get_context();

	// post type team
		$projectArgs = array(
		    // Get post type project
		    'post_type' => 'project',
		    // Get all posts
		    'posts_per_page' => 30,
		);

		$context['projectPosts'] = Timber::get_posts( $projectArgs );
	// // end post type

Timber::render('pages/work.twig', $context, 3000);

// echo TimberHelper::stop_timer( $start);